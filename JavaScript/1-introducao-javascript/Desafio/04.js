/*
4o exercício
Escreva uma função que dado um total de anos de estudo retorna o quão experiente o usuário é:
    function experiencia(anos) {
// código aqui
    }
var anosEstudo = 7;
experiencia(anosEstudo);

0-1 ano: Iniciante
1-3 anos: Intermediário
3-6 anos: Avançado
7 acima: Jedi Master*/

var anosEstudo = 7;

function experiencia(anos){
    if(anos < 2){
        return 'Iniciante';
    }else if(anos < 4){
        return 'Intermediário';
    }else if(anos < 7){
        return 'Avançado';
    }else{
        return 'Jedi Master';
    }
}

console.log(experiencia(anosEstudo));