/*
Crie uma função que dado o objeto a seguir:
    var endereco = {
        rua: "Rua dos pinheiros",
        numero: 1293,
        bairro: "Centro",
        cidade: "São Paulo",
        uf: "SP"
    };
Retorne o seguinte conteúdo:
    O usuário mora em São Paulo / SP, no bairro Centro, na rua "Rua dos Pinheiros" com
no 1293.*/

var endereco = {
    rua: "Rua dos pinheiros",
    numero: 1293,
    bairro: "Centro",
    cidade: "São Paulo",
    uf: "SP"
};

function enderecoUsuario(data){
    return `O usuário mora em ${data.cidade} / ${data.uf}, na rua "${data.rua}" com no ${data.numero}.`;
}

var usuario = enderecoUsuario(endereco);
console.log(usuario);
